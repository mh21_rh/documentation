---
title: Kernel configs for upstream kernels
weight: 50
aliases: [/l/upstream-kernel-configs]
---

Upstream kernels are built using the latest available Fedora kernel
configurations from Rawhide. This allows us to build and test kernels with a
common set of kernel configurations that are widely used.

If kernel selftests are being compiled for a kernel, `make kselftest-merge`
target is added on top of the Fedora configuration.

## Generating configs

The script for [Fedora config retrieval] can be found in the [cki-tools]
repository. The configs are uploaded to S3 lookaside storage to be readily
available for usage in the pipelines.

## Configs for local usage

The uploaded configs are *NOT* supposed to be used by end users for retriggers
or local testing as they are only a base and not the final configuration. An
extra config target (usually `olddefconfig`) is defined per-pipeline to handle
any dangling options, optionally followed by the already mentioned `kselftest-merge`
target. **Always use the config file linked in the appropriate pipeline!**

[Fedora config retrieval]: /l/kernel-config-updater
[cki-tools]: https://gitlab.com/cki-project/cki-tools/-/blob/main/shell-scripts/cki_tools_kernel_config_updater.sh
