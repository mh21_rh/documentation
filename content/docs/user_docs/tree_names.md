---
title: Standardized CKI KCIDB tree names
linkTitle: KCIDB tree names
description: How to name KCIDB trees 
weight: 80
aliases: [/l/tree-names]
---
## How to name a tree

The tree names have to follow the format below:

| Type | Format | Example |
| ---  | ---      | ---     |
| rhel y-stream | rhel-\${major\_release} | rhel-9 |
| rhel z-stream | rhel-\${major\_release}.\${minor\_release} | rhel-9.6 |
| centos | c\${release}s | c10s |
| the kernel latest fedora stable | fedora-latest | |
| kernel-ark/os-build (ELN buildroot) | fedora-eln | |
| kernel-ark/os-build (rawhide buildroot) | fedora-rawhide | |
| upstream trees | upstream-${tree\_name} | see: [kcidb_tree_name](https://gitlab.com/cki-project/pipeline-data/-/blob/main/baseline.yaml) |
