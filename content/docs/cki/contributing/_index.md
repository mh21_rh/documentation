---
title: "Contributing to the CKI project"
linkTitle: "Contributing"
description: How to work on the code of the CKI project
companion: true  # there are some internal-only contributing docs
weight: 5
---
