---
title: Planning and incident handling
description: >
    How the CKI team is using GitLab epics and issues for planning
weight: 40
---

## General idea

GitLab epics and issues are used to handle planned, unplanned and
incident-related work in a Kanban-like process.

For incidents, issues do not only cover the immediate incident response, but
also further work such as documentation improvements or root cause remediation
to prevent similar incidents.

Consistent application of the labels is enforced by the [sprinter webhook].

## Teams

Plannable work items should be tagged with `CWF::Team`. Any issue created
in any of the projects in the [cki-project] group on GitLab will be
automatically assigned a team if not done so during creation:

- `CWF::Team::ARK` for [kernel-ark], handled by the ARK team
- `CWF::Team::KWF` for [kernel-workflow], handled by the Kernel Workflow team
- `CWF::Team::CKI` for all the rest

For epics, this needs to be done manually.

To change the responsible team, change the `CWF::Team` label as appropriate.

## Structure

### Topical grouping

High-level topics can be tracked as epics with `CWF::Team` labels as
described above. These topics can be open-ended, and can either contain
individual issues or further epics.

### High-level planning

To be able to plan an Epic, its scope needs to be refined so that the **epic
represents a single deliverable feature**. After refinement, the epic should
contain the minimum set of issues tracking the actual individual work items.

Considerations:

- At least two developers should work in parallel on one epic.
- They should be sized so that it is possible to finish them within a month,
  depending on the seniority/velocity of the developers.

### Low-level planning

Individual planned work items are tracked as Issues with `CWF::Team` labels.
**Issues represent the smallest deliverable change**.

Considerations:

- Issues create visibility, which in turn enables discussion and collaboration.
- Issues allow to connect all the individual pieces required for a certain work
  item.

### Incidents

Incidents, i.e. significant user-visible service outages or degradations, are
tracked as Issue with `CWF::Type::Incident` labels.

## Life cycle of epics and issues

Epics and issues go through the following phases visible on the respective
[epic][current-quarter-board] and [issue][issue-board] boards:

- `Open`: The work item has been created and is sitting peacefully in the backlog.
- `CWF::Stage::Refined`: The team has agreed on working on the work item in the
  next planning cycle. For epics, this means that the work is considered
  critical for stake holders. For individual issues, they should be fleshed out
  enough to be implementable and have clear acceptance criteria to limit scope
  creep. If necessary, bigger issues should be split into the smallest
  deliverable changes. Both epics and issues need to be properly refined before
  they can be moved to this stage.
- `CWF::Stage::In Progress`: The work item is actively worked on.
- `Closed`: The work item is completed and all acceptance criteria are
  fulfilled. If some acceptance criteria are remaining, but no further work is
  planned for now, the outstanding acceptance criteria can also be moved into a
  new work item. Any remaining `CWF::Stage::*` labels are removed.

## Life cycle of incident issues

Incidents go through the following phases visible on the [incident issue
board][incident-board]:

- `CWF::Incident::Active`: The incident is significantly affecting the
  production environment.
- `CWF::Incident::Mitigated`: The incident is not affecting the production
  environment anymore, or only in a limited way. Some work remains to be done,
  e.g. further monitoring, documentation or root cause remediation.
- `Closed`: The issue is closed when all outstanding work items are completed.
  Any remaining `CWF::Incident::*` labels are removed.

Mitigated incidents can also be converted into normal issues if the mitigation
is considered "good enough" for now, and the remaining work is not considered
critical.

## Planning

### Planning cycle

Work is planned on a quarterly basis. This tries to satisfy the following
constraints:

- balance between feature development and maintenance work
- long enough cycle to allow to deliver multiple features
- short enough cycle to be able to deliver features within a reasonable time
  frame to stake holders

### Quarterly planning

In the following, planning means figuring out what to work on during the next
quarter.

Most likely, this also involves a cleanup of the currently existing
epics and stand-alone issues, and the adding of new plannable epics.

Various epic/issue boards are available to help with this:

- The [epic types board][epic-types-board] can be used to
  inspect and sort the currently available epics:

  1. Epics should be sorted into the correct column, with only the
     `CWF::Team::CKI` column being interesting for the acute planning.
  2. Epics in the `CWF::Team::CKI` column should be sorted with decreasing
     importance.

- The [quarterly planning epic board][quarterly-planning-board] can be used to
  further refine the "candidate epics":

  1. For the next quarter, user-requested epics should be prioritized as much
     as possible.
  2. Move epics from previous quarters that are still relevant forward, or drop
     them in the open column if no further work is planned for now.

- The [current quarter epic board][current-quarter-board] can be used to do
  fine-grained planning for the current quarter. Make sure that the board
  filter is correctly configured for the current quarter.

  1. Make sure the `CWF::Stage::In Progress` column accurately reflects the
     currently worked-on work items. There should never be more than 3 to 4
     epics in this column.
  2. The `CWF::Stage::Refined` column should contain the next epics to work on
     after that.
  3. The `Open` column should not contain anything that would be just "nice to
     have".

- The [CKI Kanban issue board][issues-without-epics] with an `Epic = None`
  filter can be used to inspect standalone issues. While it is ok to have such
  issues in the `CWF::Stage::Refined` and `CWF::Stage::In Progress` columns,
  (and the board can be used to track them across their life cycle), consider
  attaching them to an epic for the current quarter to make it easier to keep
  track of them.

- The [incident board][incident-board] shows tickets related to incidents.

  1. Aggressively purge the `CWF::Incident::Active` and
     `CWF::Incident::Mitigated` columns. It doesn't make any sense to have an
     incident sitting in these columns for a longer time. If that is the case,
     most likely the ticket is just referring to a "normal" bug, and should be
     relabeled as `CWF::Type::Bug`. Optionally, the ticket can be attached to a
     planned epic.

After the planning is done, add the `In Progress` and `Refined` epics to the
[Canvas] of the [#team-kernel-cki] Slack channel.

## Handling incidents

Next to the short-term components such as the immediate mitigation and
resolution of an incident itself, an incident response also has to contain
strategic improvements to prevent recurrence.

When handling incidents, focus on the following aspects depending on the
incident state:

1. `CWF::Incident::Active`: reduce the impact on the production environment
2. `CWF::Incident::Mitigated`: resolve the direct cause of the incident, and
   improve on the root cause of the incident, e.g. by improving
   - monitoring/alerting of the conditions that led to the incident
   - monitoring/alerting for a similar incident
   - logging to aid in faster detection/recovery for a similar incident
   - documentation
   - the underlying code and/or architecture

## Working with GitLab issues

### Creating a new issue

Create a new [GitLab issue][create-issue], e.g.

- on the top bar on a project page, select the plus sign (`+`) and then, under
  `This project`, select `New issue` from the top bar ("+")
- on the left sidebar on a project page, select `Issues` and then, in the
  upper-right corner, select `New Issue`
- on a project page, press the `i` shortcut
- on any of the GitLab issue boards, select the appropriate list
  menu (`⋮`) and then `Create new issue`

### Transitioning an issue between the different stages

To transition an issue to a different stage (`<STAGE>`), e.g.

- on the right sidebar on an issue page, select `Edit` next to `Labels`, and
  then select the appropriate `CWF::Stage::<STAGE>` label
- in the comment box on an issue page (`e` shortcut), write `/label
  ~"CWF::Stage::<STAGE>"` and submit the comment
- on any of the GitLab issue boards, drag the issue card to the appropriate
  list

### Closing an issue

Close the [GitLab issue][close-issue], e.g.

- at the top of an issue page, select `Close issue`
- in the comment box on an issue page (`e` shortcut), write `/close` and submit
  the comment
- on any of the GitLab issue boards, drag the issue card to the `Closed` list

## Working with incident issues

### Creating a new incident issue

Create a new [GitLab issue][create-issue], e.g.

- on the top bar on a project page, select the plus sign (`+`) and then, under
  `This project`, select `New issue` from the top bar ("+")
- on the left sidebar on a project page, select `Issues` and then, in the
  upper-right corner, select `New Issue`
- on a project page, press the `i` shortcut
- on the [incident issue board][incident-board], select the appropriate list
  menu (`⋮`) and then `Create new issue`

Unless the issue was created through the [incident issue
board][incident-board], make sure to tag the issue with at least
`CWF::Type::Incident`.

### Converting between an incident issue and a normal issue

To convert a normal issue into an incident issue, add the `CWF::Type::Incident`
label to it, e.g.

- on the right sidebar on an issue page, select `Edit` next to `Labels`, and
  then select the `CWF::Type::Incident` label
- in the comment box on an issue page (`e` shortcut), write `/label
  ~"CWF::Type::Incident"` and submit the comment

To convert an incident issue into a normal issue, remove the
`CWF::Type::Incident` label from it, e.g.

- on the right sidebar on an issue page, select `Edit` next to `Labels`, and
  then deselect the `CWF::Type::Incident` label
- in the comment box on an issue page (`e` shortcut), write `/unlabel
  ~"CWF::Type::Incident"` and submit the comment

### Transitioning an incident issue between the different phases

To transition an incident issue to a different phase (`<PHASE>`), e.g.

- on the right sidebar on an issue page, select `Edit` next to `Labels`, and
  then select the appropriate `CWF::Incident::<PHASE>` label
- in the comment box on an issue page (`e` shortcut), write `/label
  ~"CWF::Incident::<PHASE>"` and submit the comment
- on the [incident issue board][incident-board], drag the issue card to the
  appropriate list

### Closing an incident issue

Close the [GitLab issue][close-issue], e.g.

- at the top of an issue page, select `Close issue`
- in the comment box on an issue page (`e` shortcut), write `/close` and submit
  the comment
- on the [incident issue board][incident-board], drag the issue card to the
  `Closed` list

[sprinter webhook]: https://gitlab.com/cki-project/kernel-workflow/-/blob/main/docs/README.sprinter.md

[cki-project]: https://gitlab.com/cki-project/
[kernel-ark]: https://gitlab.com/cki-project/kernel-ark/
[kernel-workflow]: https://gitlab.com/cki-project/kernel-workflow/

[epic-types-board]: https://gitlab.com/groups/cki-project/-/epic_boards/1057665
[quarterly-planning-board]: https://gitlab.com/groups/cki-project/-/epic_boards/1057666
[current-quarter-board]: https://gitlab.com/groups/cki-project/-/epic_boards/1412

[issue-board]: https://gitlab.com/groups/cki-project/-/boards/933116
[issues-without-epics]: https://gitlab.com/groups/cki-project/-/boards/933116?epic_id=None
[incident-board]: https://gitlab.com/groups/cki-project/-/boards/4642958

[create-issue]: https://docs.gitlab.com/ee/user/project/issues/create_issues.html
[close-issue]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#close-an-issue

[Canvas]: https://redhat.enterprise.slack.com/canvas/C04KPCFGDTN
[#team-kernel-cki]: https://redhat.enterprise.slack.com/archives/C04KPCFGDTN

<!-- vi: set spell spelllang=en: -->
